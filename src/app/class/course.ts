import {Material} from './material';

export class Course {

  code: string = null;
  name: string = null;
  url: string = null;
  material: Material[] = [];

  constructor(json?: any){
    if(json) {
      this.code = json.code;
      this.name = json.name;
      this.url = json.url;
      this.material = json.material;
    }
  }

  populate(value: Course): Course{
    this.code = value.code;
    this.name = value.name;
    this.url = value.url;
    this.material = value.material;

    return this;
  }

}
