import {AbstractPaginator} from './abstract-paginator';
import {HttpResponseInterface} from '../interface/http-response.interface';
import {Course} from '../class/course';

export class CoursesPaginator extends AbstractPaginator {
  content: Course[] = [];

  constructor(response?: HttpResponseInterface) {
    super(response);
    if (response) {
      const res: Course[] = [];
      for (let i = 0; i < response.content.length; i++) {
        res.push(new Course(response.content[i]));
      }

      this.content = res;
    }
  }

}
