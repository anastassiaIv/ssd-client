//== Class definition
var Typeahead = function() {

    var services = ['Hommikusöök otse hotellituppa kahele', 'Saabumisel toas puuviljad', 'Saabumisel toas vahuvein', 'Hiline väljaregistreerimine kuni kell 14.00', 'Kuni 12-aastane laps ööbib tasuta',
        'Tasuta wifi', 'SPA'
    ];

    var roomTypes = ['Mullivanniga deluxe kahekohaline tuba', 'Superior kahekohaline tuba', 'Superiorklassi tuba'];

    var extraBedTypes = ['Babyvoodi', 'Laste lisavoodi', 'Täiskasvanute lisavoodi'];


    //== Private functions
    var demo1 = function() {
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;
                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };

        $('#m_typeahead_1, #m_typeahead_1_modal, #m_typeahead_1_validate, #m_typeahead_2_validate, #m_typeahead_3_validate').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'services',
            source: substringMatcher(services)
        });

        $('#m_typeahead_2').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'roomTypes',
            source: substringMatcher(roomTypes)
        });

        $('#m_typeahead_3').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'extraBedTypes',
            source: substringMatcher(extraBedTypes)
        });
    }


    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

jQuery(document).ready(function() {
    Typeahead.init();
});