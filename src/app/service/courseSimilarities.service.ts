import {Injectable} from '@angular/core';
import {HttpService} from './http/http.service';
import {HttpClient} from '@angular/common/http';
import {Config} from '../config/config';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Similarity} from '../class/similarity';

@Injectable()
export class CourseSimilaritiesService extends HttpService {

  public static SIMILARITY_URL = '/courseSimilarities';

  constructor(httpClient: HttpClient, config: Config, router: Router) {
    super(httpClient, config, router);
  }

  public getSimilarityByCode(code: string): Observable<Similarity[]> {
    return super.getList(CourseSimilaritiesService.SIMILARITY_URL + '/similarities/' + code).pipe(
      map((response: any) => {
        return response;
      }));
  }
}
