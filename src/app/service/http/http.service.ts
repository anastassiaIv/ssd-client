import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Config } from '../../config/config';
import {Observable, SchedulerLike, Subscriber} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class HttpService {

  constructor(protected httpClient: HttpClient, protected config: Config, protected router: Router) { }

  public getList(url: string, params?: HttpParams): Observable<any[]> {
    return this.intercept(this.httpClient.get<any[]>(this.getApiUrl(url), {params}));
  }

  public post(url: string, data: string): Observable<any> {
    return this.intercept(this.httpClient.post<any>(this.getApiUrl(url), data));
  }

  private getApiUrl(url: string): string {
    return this.config.getApiUrl(url);
  }

  private intercept(observable: Observable<any>): Observable<any> {
    return observable.pipe(
      catchError((err, source) => {
      if (err.status === 401 ) {
      }
      return throwError(err);
    }));
  }
}

export function throwError(error: any, scheduler?: SchedulerLike): Observable<never> {
  if (!scheduler) {
  return new Observable(subscriber => subscriber.error(error));
} else {
  return new Observable(subscriber => scheduler.schedule(dispatch, 0, { error, subscriber }));
}
}

interface DispatchArg {
  error: any;
  subscriber: Subscriber<any>;
}

function dispatch({ error, subscriber }: DispatchArg) {
  subscriber.error(error);
}
