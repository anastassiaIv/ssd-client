import {BehaviorSubject, Observable, of} from 'rxjs';
import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {AbstractDataSource} from './abstract-data-source';
import {catchError, finalize} from 'rxjs/operators';
import {Material} from '../class/material';
import {MaterialListFilter} from '../filter/materialListFiltert';
import {MaterialService} from '../service/material.service';

export class MaterialDataSource extends AbstractDataSource implements DataSource<Material> {

  private materialSubject = new BehaviorSubject<Material[]>([]);
  public content: Material[];
  public total = 0;

  constructor(private infoService: MaterialService) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<Material[]> {
    return this.materialSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.materialSubject.complete();
    this.loadingSubject.complete();
  }

  loadMaterials(filter: MaterialListFilter) {

    this.loadingSubject.next(true);

    return this.infoService.getMaterialsByCourseList(filter.getHttpParams()).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe((pag: Material[]) => {
        this.content = pag;
        this.materialSubject.next(pag);
      });
  }
}
