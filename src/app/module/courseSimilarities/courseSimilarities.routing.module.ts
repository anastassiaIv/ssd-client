import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CourseSimilaritiesListComponent} from './component/list/courseSimilarities-list.component';
import {CourseSimilaritiesSimilarityComponent} from './component/similarities/courseSimilarities-similarity.component';


const routes: Routes = [
  {
    path: '',
    component: CourseSimilaritiesListComponent
  },
  {
    path: 'similarities/:id',
    component: CourseSimilaritiesSimilarityComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseSimilaritiesRoutingModule { }
