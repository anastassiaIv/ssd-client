//== Class definition

var DatatableHtmlTableDemo = function() {
  //== Private functions

  // demo initializer
  var demo = function() {

    var datatable = $('.m-datatable').mDatatable({

      data: {
        saveState: {cookie: false}
      },
      search: {
        input: $('#generalSearch')
      },
      columns: [
          {
              field: "RecordID",
              title: "#",
              width: 30,
              sortable: false,
              textAlign: 'center',
              selector: {class: 'm-checkbox--solid m-checkbox--brand'}
          },
        {
          field: 'Nimetus',
          width: 300
        },
      {
          field: 'Hinded',
          width: 250
      },
      {
          field: 'Koondhinne',
          width: 110
      },
        {
          field: 'Toatüübid',
          width: 70
        },
      {
          field: 'Kehtivus',
          width: 100
      },
      {
          field: 'Br nr',
          width: 80
      },
      {
          field: 'Hind',
          width: 120
      },
      {
          field: 'Pakett',
          width: 250
      },
      {
          field: 'Paketi staatus',
          width: 120
      },
      {
          field: '',
          width: 120,
          textAlign: 'right'
      },
      ]
    });

  };

    var demo2 = function() {


        var datatable2 = $('.m-datatable2').mDatatable({

            data: {
                saveState: {cookie: false}
            },
            search: {
                input: $('#generalSearch')
            },
            columns: [
                {
                    field: "RecordID",
                    title: "#",
                    width: 30,
                    sortable: false,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'Nimetus',
                    width: 300
                },
                {
                    field: 'Toatüübid',
                    width: 70
                },
                {
                    field: 'Kehtivus',
                    width: 100
                },
                {
                    field: 'Br nr',
                    width: 80
                },
                {
                    field: 'Pakett',
                    width: 250
                },
                {
                    field: 'ID',
                    width: 80
                },
                {
                    field: '',
                    width: 120,
                    textAlign: 'right'
                },
            ]
        });

    };

    var demo3 = function() {

        var datatable3 = $('.m-datatable3').mDatatable({

            data: {
                saveState: {cookie: false}
            },
            search: {
                input: $('#generalSearch')
            },
            columns: [
                {
                    field: "RecordID",
                    title: "#",
                    width: 30,
                    sortable: false,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'Nimetus',
                    width: 300
                },
                {
                    field: 'Toatüübid',
                    width: 70
                },
                {
                    field: 'Kehtivus',
                    width: 100
                },
                {
                    field: 'Br nr',
                    width: 80
                },
                {
                    field: 'Pakett',
                    width: 250
                },
                {
                    field: 'ID',
                    width: 80
                },
                {
                    field: '',
                    width: 120,
                    textAlign: 'right'
                },
            ]
        });
    };

    var demo4 = function() {

        var datatable4 = $('.m-datatable4').mDatatable({

            data: {
                saveState: {cookie: false}
            },
            search: {
                input: $('#generalSearch')
            },
            columns: [
                {
                    field: "RecordID",
                    title: "#",
                    width: 30,
                    sortable: false,
                    textAlign: 'center',
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                },
                {
                    field: 'Nimetus',
                    width: 300
                },
                {
                    field: 'Toatüübid',
                    width: 70
                },
                {
                    field: 'Kehtivus',
                    width: 100
                },
                {
                    field: 'Br nr',
                    width: 80
                },
                {
                    field: 'Pakett',
                    width: 250
                },
                {
                    field: 'ID',
                    width: 80
                },
                {
                    field: '',
                    width: 120,
                    textAlign: 'right'
                },
            ]
        });
    };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo();
        demo2();
        demo3();
        demo4();
    }
  };
}();

jQuery(document).ready(function() {
  DatatableHtmlTableDemo.init();
})
