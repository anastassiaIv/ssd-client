import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NavigationDto, SubNavigationDto} from '../../../class/navigation';

declare let mLayout: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class HeaderComponent implements OnInit, AfterViewInit {

  title: string;
  titleIconClass: string;
  subNavigations: NavigationDto[] = [];

  constructor() {}

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    mLayout.initHeader();
  }

  public setTitleAndSubNav(subNavDto: SubNavigationDto): void {
    this.title = subNavDto.title;
    this.titleIconClass = subNavDto.titleIconClass;
    this.subNavigations = subNavDto.navigations;
  }

}
