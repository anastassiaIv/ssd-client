import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {AbstractCourseSimilaritiesComponent} from '../abstract-courseSimilarities.component';
import {NavigationService} from '../../../../service/navigation.service';
import {AppComponent} from '../../../../app.component';
import {ScriptLoaderService} from '../../../../service/script-loader.service';
import {ErrorService} from '../../../../service/error.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Subscription} from 'rxjs/internal/Subscription';
import {MatPaginator, MatSort} from '@angular/material';
import {PageEvent} from '@angular/material/typings/esm5/paginator';
import {CoursesListFilter} from '../../../../filter/coursesListFilter';
import {CoursesService} from '../../../../service/courses.service';
import {CoursesDataSource} from '../../../../data-source/courses-data-source';

@Component({
  selector: 'app-courseSimilarities-list',
  templateUrl: './courseSimilarities-list.component.html'
})
export class CourseSimilaritiesListComponent extends AbstractCourseSimilaritiesComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns = ['code', 'name', 'url', 'custom'];
  dataSource: CoursesDataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  filterModel: CoursesListFilter = new CoursesListFilter();
  private sub: Subscription;

  constructor(appComp: AppComponent, navService: NavigationService, scriptLoaderService: ScriptLoaderService,
              private coursesService: CoursesService, private errorService: ErrorService,
              private renderer: Renderer2, private elementRef: ElementRef, private router: Router, private route: ActivatedRoute) {
    super(appComp, navService, scriptLoaderService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.dataSource = new CoursesDataSource(this.coursesService);
    this.route.params.subscribe(
      (params: Params) => {
        this.dataSource.loadCoursesNotDetailed(this.filterModel.updateFromParams(params));
      }
    );
  }

  ngAfterViewInit() {
    this._script.loadScripts('app-courseSimilarities-list',
      [
        'assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js',
        'assets/vendors/custom/datatables/datatables.bundle.js',
        'assets/demo/default/custom/crud/datatables/basic/paginations.js'
      ]);
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  filter() {
    this.router.navigate(['courseSimilarities', this.filterModel.getQueryParams()]);
  }


  filterResult() {
    this.filterModel.paginator.reset();
    this.navigate();
  }

  getResult(event?: PageEvent) {
    if (event) {
      this.filterModel.paginator.updateFromPageEvent(event);
    }

    this.navigate();
  }

  navigate() {
    this.router.navigate(['courseSimilarities', this.filterModel.getQueryParams()]);
  }

  getDistances(code: string) {

  }

  getCurrentSemester() {
    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth() + 1;
    let sem = '';
    if (month > 1 && month < 9) {
      sem = 'S';
    } else {
      sem = 'F';
    }
    return year.toString() + sem;
  }


}
