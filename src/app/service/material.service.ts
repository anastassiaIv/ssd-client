import {HttpService} from './http/http.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpParams} from '@angular/common/http';
import {HttpResponseInterface} from '../interface/http-response.interface';
import {Material} from '../class/material';

@Injectable()
export class MaterialService extends HttpService {

  public static MATERIAL_URL = '/materials';

  public getMaterialsByCourseList(params: HttpParams): Observable<Material[]> {
    return super.getList(MaterialService.MATERIAL_URL + '/list', params).pipe(
      map((response: any) => {
        return response;
      }));
  }

}
