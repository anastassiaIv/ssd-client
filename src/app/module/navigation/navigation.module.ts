import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {MainNavigationComponent} from './component/main-navigation/main-navigation.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    MainNavigationComponent
  ],
  exports: [
    MainNavigationComponent
  ]
})
export class NavigationModule { }
