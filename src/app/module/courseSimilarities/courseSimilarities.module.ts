import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {CourseSimilaritiesRoutingModule} from './courseSimilarities.routing.module';
import {FormsModule} from '@angular/forms';
import {MatPaginatorModule,  MatSelectModule, MatTableModule} from '@angular/material';
import {PipeModule} from '../pipe/pipe.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgxGraphModule} from '@swimlane/ngx-graph';
import {CourseSimilaritiesSimilarityComponent} from './component/similarities/courseSimilarities-similarity.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {CourseSimilaritiesListComponent} from './component/list/courseSimilarities-list.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    CourseSimilaritiesRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    PipeModule,
    MatSelectModule,
    NgSelectModule,
    NgxGraphModule,
    NgxChartsModule,
    NgxSpinnerModule
  ],
  declarations: [
    CourseSimilaritiesListComponent,
    CourseSimilaritiesSimilarityComponent
  ],
  exports: [
    CourseSimilaritiesListComponent,
    CourseSimilaritiesSimilarityComponent
  ]
})
export class CourseSimilaritiesModule { }
