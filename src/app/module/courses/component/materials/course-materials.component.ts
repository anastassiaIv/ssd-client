import {Component, Input, OnInit} from '@angular/core';
import {Course} from '../../../../class/course';

@Component({
  selector: 'app-course-materials',
  templateUrl: './course-materials.component.html'
})
export class CourseMaterialsComponent implements OnInit {

  @Input() materials: any;
  @Input() course: Course = new Course();

  constructor() {}

  ngOnInit() {
  }

  public setCourse(course: Course) {
    this.course = course;
  }

}
