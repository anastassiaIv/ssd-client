import {HttpParams} from '@angular/common/http';
import {Params} from '@angular/router';
import {CoursesPaginator} from '../paginator/courses-paginator';

export class CoursesListFilter {
  paginator: CoursesPaginator = new CoursesPaginator();
  query: string = '';
  code: string;

  updateFromParams(params?: Params) {
    if (params) {
      this.paginator.updateFromParams(params);
      if (params.query) { this.query = params.query; }
    }

    return this;
  }


  getHttpParams() {
    const params = new HttpParams()
      .set('code', this.code);

    return params;
  }


  getQueryParams() {
    const data = {};
    if (this.code && this.code.length > 0) {
      data['code'] = this.code;
    }
    return data;
  }
}
