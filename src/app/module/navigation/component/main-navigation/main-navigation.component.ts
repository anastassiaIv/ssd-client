import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html'
})
export class MainNavigationComponent implements OnInit {

  courses: boolean;
  courseSimilarities: boolean;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.detectRouteChange();
  }

  private detectRouteChange() {
    this.router.events.subscribe((val) => {

      if (val instanceof NavigationEnd) {
        this.resetActives();
        if (val.url.indexOf('courses') > -1) {
          this.courses = true;
        }
        if (val.url.indexOf('courseSimilarities') > -1) {
          this.courseSimilarities = true;
        }
      }
    });
  }

  private resetActives() {
    this.courses = false;
    this.courseSimilarities = false;
  }
}
