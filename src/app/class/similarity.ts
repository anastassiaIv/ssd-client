export class Similarity {

  course: string = null;
  similarity: number;

  constructor(json?: any) {
    if (json) {
      this.course = json.course;
      this.similarity = json.similarity;
    }
  }
}
