import { Injectable } from '@angular/core';
import {ConfigData} from './config-data';

@Injectable()
export class Config {

  public getApiUrl(uriEnd: string){
    return ConfigData.CONFIG_DATA.backendUri + uriEnd;
  }
}
