import {Injectable} from '@angular/core';
import {HttpService} from './http/http.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Config} from '../config/config';
import {Router} from '@angular/router';
import {Course} from '../class/course';

@Injectable()
export class CoursesService extends HttpService {

  public static COURSES_URL = '/courses';

  constructor(httpClient: HttpClient,  config: Config,  router: Router) {
    super(httpClient, config, router);
  }

  public getList(): Observable<any> {
    return super.getList(CoursesService.COURSES_URL);
  }

  public getCoursesAllList(params: HttpParams): Observable<Course[]>  {
    return super.getList(CoursesService.COURSES_URL + '/all', params).pipe(
      map(response => {
        if (response) {
          const res: Course[] = [];
          for (let i = 0; i < response.length; i++) {
            res.push(new Course(response[i]));
          }

          return res;
        }
      })
    );
  }
}
