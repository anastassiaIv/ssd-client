import {Injectable} from '@angular/core';
import {SubNavigationDto} from '../class/navigation';


@Injectable()
export class NavigationService {


  public getCoursesNavigationSubNavigation() {
    return new SubNavigationDto(
      'Kursused',
      'flaticon-settings-1',
      []
    );
  }

  public getCourseSuggestionsNavigationSubNavigation() {
    return new SubNavigationDto(
      'Seosed teiste ainetega',
      'flaticon-settings-1',
      []
    );
  }


}
