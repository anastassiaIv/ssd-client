import {HttpParams} from '@angular/common/http';

export class MaterialListFilter {
  code: string;


  getHttpParams() {
    const params = new HttpParams()
      .set('code', this.code);

    return params;
  }
}
