import {ElementRef, Injectable} from '@angular/core';

@Injectable()
export class JsLibService {

  public static errorMessageDiv = '<div style="color: red" class="form-control-feedback help-block"><div class="err-message">{message}</div></div>';

  validateForm(element: ElementRef, rules: any, messages: any){

    (<any>jQuery(element.nativeElement)).validate({
      errorElement: 'div',
      onkeyup: false,
      onclick: false,
      ignore: [],
      rules: rules,
      messages: messages,
      errorPlacement: function (error: any, element: any) {
        const formGroupDiv = jQuery(element).closest('div[class^="form-group"]');
        const helpBlock1 = formGroupDiv.find('.help-block');
        if(!helpBlock1 || helpBlock1.length === 0){
          formGroupDiv.append(JsLibService.errorMessageDiv.replace('{message}', error.text()));
        } else {
          const messageBlock = helpBlock1.find('.err-message');
          messageBlock.html(error.text());
        }
      },
      highlight: function (element: any) {
        jQuery(element).closest('div[class^="form-group"]').addClass('has-danger');
      },
      unhighlight: function (element: any) {
        jQuery(element).closest('div[class^="form-group"]').removeClass('has-danger');
        jQuery(element).closest('div[class^="form-group"]').find('.help-block').remove();
      }
    });
  }

  isValid(element: ElementRef){
    return (<any>jQuery(element.nativeElement)).valid();
  }

  validateEmail(email: string) {
    const expression = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (expression.test(email)) {
      return true;
    } else {
      return false;
    }
  }
}
