export class SubNavigationDto {
  title: string;
  titleIconClass: string;
  navigations: NavigationDto[] = [];

  constructor(title: string, titleIconClass: string, navigations: NavigationDto[]){
    this.title = title;
    this.titleIconClass = titleIconClass;
    this.navigations = navigations;
  }
}

export class NavigationDto {

  href: string;
  name: string;

  constructor(href: string, name: string) {
    this.href = href;
    this.name = name;
  }
}
