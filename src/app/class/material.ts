export class Material {

  title: string = null;
  link: string = null;

  constructor(json?: any) {
    if (json) {
      this.title = json.title;
      this.link = json.link;
    }
  }
}
