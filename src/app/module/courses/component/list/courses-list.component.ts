import {AfterViewInit, Component,  OnDestroy, OnInit,  ViewChild} from '@angular/core';
import {CoursesService} from '../../../../service/courses.service';
import {ErrorService} from '../../../../service/error.service';
import {NavigationService} from '../../../../service/navigation.service';
import {AppComponent} from '../../../../app.component';
import {Course} from '../../../../class/course';
import {ScriptLoaderService} from '../../../../service/script-loader.service';
import {Subscription} from 'rxjs/internal/Subscription';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MatPaginator, MatSort} from '@angular/material';
import {PageEvent} from '@angular/material/typings/esm5/paginator';
import {AbstractCoursesComponent} from '../abstract-courses.component';
import {CoursesDataSource} from '../../../../data-source/courses-data-source';
import {CoursesListFilter} from '../../../../filter/coursesListFilter';
import {MaterialListFilter} from '../../../../filter/materialListFiltert';
import {MaterialService} from '../../../../service/material.service';
import {Material} from '../../../../class/material';
import {CourseMaterialsComponent} from '../materials/course-materials.component';
import {NgxSpinnerService} from 'ngx-spinner';

declare var jQuery: any;

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html'
})
export class CoursesListComponent extends AbstractCoursesComponent implements OnInit, AfterViewInit, OnDestroy {

  course = new Course();

  filterModel: CoursesListFilter;
  materialFilterModel: MaterialListFilter;
  private sub: Subscription;
  loaded = false;
  displayedColumns = ['code', 'name', 'url', 'custom'];
  dataSource: CoursesDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  materials = {};

  @ViewChild(CourseMaterialsComponent) courseMaterialsComponent: CourseMaterialsComponent;


  constructor(appComp: AppComponent, navService: NavigationService, private coursesService: CoursesService, private errorService: ErrorService,
              scriptLoaderService: ScriptLoaderService, private router: Router, private route: ActivatedRoute, private materialService: MaterialService,
              private spinner: NgxSpinnerService) {
    super(appComp, navService, scriptLoaderService);
  }

  ngOnInit() {
    this.filterModel = new CoursesListFilter();
    this.materialFilterModel = new MaterialListFilter();
    super.ngOnInit();
    this.dataSource = new CoursesDataSource(this.coursesService);
    this.route.params.subscribe(
      (params: Params) => {
        this.dataSource.loadCoursesNotDetailed(this.filterModel.updateFromParams(params));
      }
    );
  }

  ngAfterViewInit() {
    this._script.loadScripts('app-courses-list',
      [
        'assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js',
        'assets/vendors/custom/datatables/datatables.bundle.js',
        'assets/demo/default/custom/crud/datatables/basic/paginations.js'
      ]);

  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  navigate() {
    this.router.navigate(['courses', this.filterModel.getQueryParams()]);
  }

  searchMaterial(code: string) {
    console.log('searching...' + code);
    this.spinner.show();
    this.materialFilterModel.code = code;
    this.materialService.getMaterialsByCourseList(this.materialFilterModel.getHttpParams()).subscribe(
      (res: Material[]) => {
        this.materials[code] = res;
        this.spinner.hide();
      }, (err) => {
        this.spinner.hide();
        this.errorService.showError(err);
      }
    );
  }

  getCurrentSemester() {
    const year = (new Date()).getFullYear();
    const month = (new Date()).getMonth() +  1;
    let sem = '';
    if (month > 1 && month < 9) {
      sem = 'S';
    } else {
      sem = 'F';
    }
    return year.toString() + sem;
  }

  setFormCourse(course?: Course) {
    const value = new Course();
    this.courseMaterialsComponent.setCourse(course != null ? value.populate(course) : value);
  }

}
