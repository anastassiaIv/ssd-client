//== Class definition

var BootstrapDaterangepicker = function () {
    
    //== Private functions
    var demos = function () {
        // minimum setup
        $('#m_daterangepicker_1, #m_daterangepicker_1_modal').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        });

        // input group and left alignment setup
        $('#m_daterangepicker_2').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#m_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

         $('#m_daterangepicker_2_modal').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#m_daterangepicker_2 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

        // left alignment setup
        $('#m_daterangepicker_3').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#m_daterangepicker_3 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

        $('#m_daterangepicker_3_modal').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#m_daterangepicker_3 .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });


        // date & time
        $('#m_daterangepicker_4').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        }, function(start, end, label) {
            $('#m_daterangepicker_4 .form-control').val( start.format('MM/DD/YYYY h:mm A') + ' / ' + end.format('MM/DD/YYYY h:mm A'));
        });

        // date picker
        $('#m_daterangepicker_5').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            singleDatePicker: true,
            showDropdowns: true,

            locale: {
                format: 'MM/DD/YYYY'
            }
        }, function(start, end, label) {
            $('#m_daterangepicker_5 .form-control').val( start.format('MM/DD/YYYY') + ' / ' + end.format('MM/DD/YYYY'));
        });

        // predefined ranges
        var start = moment().subtract(29, 'days');
        var end = moment();

        $('#m_daterangepicker_6').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            alwaysShowCalendars: true,
            showCustomRangeLabel: false,

            startDate: start,
            endDate: end,

            locale: {
                applyLabel: "Vali",
                customRangeLabel: "Kalender",
                cancelLabel: "Sulge"
            },

            ranges: {
                'Järgmised 30 päeva': [moment().subtract(29, 'days'), moment()],
                'Jooksev kuu': [moment().startOf('month'), moment().endOf('month')],
               'Järgmised 7 päeva': [moment().subtract(6, 'days'), moment()],
               'Järgmine 3 kuud': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Jooksev aasta': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function(start, end, label) {
            $('#m_daterangepicker_6 .form-control').val( start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
        });

        $('#m_daterangepicker_7').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            alwaysShowCalendars: true,
            showCustomRangeLabel: false,

            startDate: start,
            endDate: end,

            locale: {
                applyLabel: "Vali",
                customRangeLabel: "Kalender",
                cancelLabel: "Sulge"
            },

            ranges: {
                'Järgmised 30 päeva': [moment().subtract(29, 'days'), moment()],
                'Jooksev kuu': [moment().startOf('month'), moment().endOf('month')],
                'Järgmised 7 päeva': [moment().subtract(6, 'days'), moment()],
                'Järgmine 3 kuud': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Jooksev aasta': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function(start, end, label) {
            $('#m_daterangepicker_7 .form-control').val( start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
        });

        $('#m_daterangepicker_8').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            alwaysShowCalendars: true,
            showCustomRangeLabel: false,

            startDate: start,
            endDate: end,

            locale: {
                applyLabel: "Vali",
                customRangeLabel: "Kalender",
                cancelLabel: "Sulge"
            },

            ranges: {
                'Järgmised 30 päeva': [moment().subtract(29, 'days'), moment()],
                'Jooksev kuu': [moment().startOf('month'), moment().endOf('month')],
                'Järgmised 7 päeva': [moment().subtract(6, 'days'), moment()],
                'Järgmine 3 kuud': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Jooksev aasta': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, function(start, end, label) {
            $('#m_daterangepicker_8 .form-control').val( start.format('DD.MM.YYYY') + ' - ' + end.format('DD.MM.YYYY'));
        });
    }

    var validationDemos = function() {
        // input group and left alignment setup
        $('#m_daterangepicker_1_validate').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#m_daterangepicker_1_validate .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

        // input group and left alignment setup
        $('#m_daterangepicker_2_validate').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#m_daterangepicker_3_validate .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });

        // input group and left alignment setup
        $('#m_daterangepicker_3_validate').daterangepicker({
            buttonClasses: 'm-btn btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary'
        }, function(start, end, label) {
            $('#m_daterangepicker_3_validate .form-control').val( start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
        });        
    }

    return {
        // public functions
        init: function() {
            demos(); 
            validationDemos();
        }
    };
}();

jQuery(document).ready(function() {    
    BootstrapDaterangepicker.init();
});