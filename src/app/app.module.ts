import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { Config } from './config/config';
import { AppComponent } from './app.component';
import {HttpService} from './service/http/http.service';
import {AppRoutingModule} from './app-routing.module';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

import {Router} from '@angular/router';
import {CoursesService} from './service/courses.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {ErrorService} from './service/error.service';
import {HeaderFooterModule} from './module/header-footer/header-footer.module';
import {NavigationModule} from './module/navigation/navigation.module';
import {NavigationService} from './service/navigation.service';
import {ScriptLoaderService} from './service/script-loader.service';
import {JsLibService} from './service/js-lib.service';
import {MaterialService} from './service/material.service';
import {CourseSimilaritiesService} from './service/courseSimilarities.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    HeaderFooterModule,
    NavigationModule,
    BrowserAnimationsModule, // required animations module
    NgbModule.forRoot(),
    ToastrModule.forRoot({
      preventDuplicates: true,
      enableHtml: true
    })
  ],
  providers: [
    ScriptLoaderService,
    CoursesService,
    ErrorService,
    NavigationService,
    Config,
    JsLibService,
    MaterialService,
    CourseSimilaritiesService,
    {
      provide: HttpService,
      useFactory: (client: HttpClient, conf: Config, router: Router) =>
        new HttpService(client, conf, router),
      deps: [HttpClient, Config, Router]
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
