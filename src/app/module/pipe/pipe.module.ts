import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {LinkifyPipe} from '../../pipe/linkify.pipe';
import {TextWrapPipe} from '../../pipe/text-wrap.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LinkifyPipe,
    TextWrapPipe
  ],
  exports: [
    LinkifyPipe,
    TextWrapPipe
  ]
})
export class PipeModule { }
