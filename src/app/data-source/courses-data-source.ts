import {BehaviorSubject, Observable, of} from 'rxjs';
import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {AbstractDataSource} from './abstract-data-source';
import {catchError, finalize} from 'rxjs/operators';
import {Course} from '../class/course';
import {CoursesPaginator} from '../paginator/courses-paginator';
import {CoursesListFilter} from '../filter/coursesListFilter';
import {CoursesService} from '../service/courses.service';

export class CoursesDataSource extends AbstractDataSource implements DataSource<Course> {

  private coursesSubject = new BehaviorSubject<Course[]>([]);
  public content: Course[];
  public total = 0;

  constructor(private infoService: CoursesService) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<Course[]> {
    return this.coursesSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.coursesSubject.complete();
    this.loadingSubject.complete();
  }

  loadCoursesNotDetailed(filter: CoursesListFilter) {

    this.loadingSubject.next(true);

    return this.infoService.getCoursesAllList(filter.getHttpParams()).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe((pag: Course[]) => {
        this.content = pag;
        this.coursesSubject.next(this.content);
      });
  }
}
