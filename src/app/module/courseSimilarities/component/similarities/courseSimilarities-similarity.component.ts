import {Component, OnInit} from '@angular/core';
import {AbstractCourseSimilaritiesComponent} from '../abstract-courseSimilarities.component';
import {AppComponent} from '../../../../app.component';
import {NavigationService} from '../../../../service/navigation.service';
import {ErrorService} from '../../../../service/error.service';
import {ScriptLoaderService} from '../../../../service/script-loader.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Similarity} from '../../../../class/similarity';
import {CourseSimilaritiesService} from '../../../../service/courseSimilarities.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-courseSimilarities-similarity',
  templateUrl: './courseSimilarities-similarity.component.html'
})
export class CourseSimilaritiesSimilarityComponent extends AbstractCourseSimilaritiesComponent implements OnInit {

  similarities: Similarity[];
  code: string;

  constructor(appComp: AppComponent, navService: NavigationService, private errorService: ErrorService,
              private similarityService: CourseSimilaritiesService, scriptLoaderService: ScriptLoaderService,
              private route: ActivatedRoute, private router: Router, private spinner: NgxSpinnerService) {
    super(appComp, navService, scriptLoaderService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.spinner.show();
    this.code = String(this.route.snapshot.paramMap.get('id'));
    this.similarityService.getSimilarityByCode(this.code).subscribe(
      (res: Similarity[]) => {
        this.similarities = res;
        if (this.similarities && this.similarities.length > 0) {
          this.spinner.hide();
        } else {
          this.navigate();
          this.errorService.showError('No comparison found');
        }
      }, (err) => {
        this.errorService.showError(err);
      }
    );
  }

  navigate() {
    this.router.navigate(['courseSimilarities']);
  }
}
