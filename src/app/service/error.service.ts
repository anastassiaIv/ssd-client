import {Injectable} from '@angular/core';

import {ToastrService} from 'ngx-toastr';


@Injectable()
export class ErrorService {
  private errorDto = new ErrorDto();

  constructor(private toastr: ToastrService) {}

  public showError(errorObject: any) {
    if (errorObject) {
      const message = this.composeError(errorObject);
      if (message && message.length > 0) {
        this.toastr.error(message);
      }
    }
  }

  public composeError(errorObject: any) {
    try {
      const response = errorObject.error;
      if (errorObject['status'] === 0 || errorObject['status'] === 502 || response instanceof ProgressEvent) {
        return 'Teenus pole hetkel kättesaadav. Palun proovige mõne aja pärast uuesti.';
      }
      this.errorDto.populate(response);
      return this.createErrorMessage();
    } catch (e) {
      console.log(e);
    }
  }

  private createErrorMessage() {
    let message = `<div>` + 'Teadmata viga!' + `</div></br>`;
    if (this.errorDto.message) {
      message += '<div>' + this.errorDto.message + '</div></br>';
    }
    if (this.errorDto.id) {
      message += '<div>ID: ' + this.errorDto.id + '</div></br>';
    }
    if (this.errorDto.entity) {
      const value = this.errorDto.value.length === 0 && (this.errorDto.field === 'id' || this.errorDto.field === 'code')
        ? this.errorDto.id : this.errorDto.value;
    }
    if (this.errorDto.rows.length > 0) {
      message += `
                    <div>Tekkisid järgmised vead:</div></br>
                    <table class="table table-inverse">
                        <tr>{{ERRORS}}</tr>
                    </table>
                `;
      let errors = '';
      if (this.errorDto.rows.length === 1 && this.errorDto.rows[0].field) {
        const splited = this.errorDto.rows[0].field.split('|');
        for (let i = 0; i < splited.length; i++) {
          errors +=
            `
                            <tr>
                                <td>` + splited[i] + `</td>
                                <td>` + this.errorDto.rows[0].code + `</td>
                            </tr>
                        `;

        }
        if (this.errorDto.rows[0].explanation && this.errorDto.rows[0].explanation.length > 0) {
          errors +=
            `
                            <tr>
                                <td colspan="2">` + this.errorDto.rows[0].explanation + `</td>
                            </tr>
                        `;
        }
      } else {
        for (let i = 0; i < this.errorDto.rows.length; i++) {
          errors +=
            `
                            <tr>
                                <td>` + this.errorDto.rows[i].field + `</td>
                                <td>` + this.errorDto.rows[i].code + `</td>
                            </tr>
                        `;

          if (this.errorDto.rows[i].explanation && this.errorDto.rows[i].explanation.length > 0) {
            errors +=
              `
                            <tr>
                                <td colspan="2">` + this.errorDto.rows[i].explanation + `</td>
                            </tr>
                        `;
          }
        }
      }

      message = message.replace('{{ERRORS}}', errors);
    }

    return message;
  }

}

export class ErrorDto {
  id: string;
  code: string;
  message: string;
  entity = '';
  field = '';
  value = '';
  rows: ErrorRowDto[] = [];

  constructor() {}

  populate(response: any) {
    this.reset();
    if (response.hasOwnProperty('id')) {
      this.id = response.id;
    }
    if (response.hasOwnProperty('code')) {
      this.code = response.code;
    }
    if (response.hasOwnProperty('message')) {
      this.message = response.message;
    }
    if (response.hasOwnProperty('entity')) {
      this.entity = response.entity;
    }
    if (response.hasOwnProperty('field')) {
      this.field = response.field;
    }
    if (response.hasOwnProperty('value')) {
      this.value = response.value;
    }
    if (response.hasOwnProperty('rows')) {
      for (let i = 0; i < response.rows.length; i++) {
        this.rows.push(new ErrorRowDto(response.rows[i].field, response.rows[i].code, response.rows[i].explanation));
      }
    }
  }

  reset() {
    this.id = null;
    this.code = null;
    this.message = null;
    this.entity = '';
    this.field = '';
    this.value = '';
    this.rows = [];
  }
}

export class ErrorRowDto {
  field = '';
  code = '';
  explanation: string;

  constructor(field: string, code: string, explanation?: string) {
    this.field = field;
    this.code = code;
    this.explanation = explanation;
  }
}
