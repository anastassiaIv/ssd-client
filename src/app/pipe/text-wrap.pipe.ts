import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'textWrap', pure: false})
export class TextWrapPipe implements PipeTransform {

  transform(text?: any, length?: number): any {
    if (!text || text === '') {
      return '';
    } else {
      return text.length <= length ? text : text.substring(0, length) + '...';
    }

  }
}
