
import {HttpResonseInterface} from '../interface/http-resonse.interface';
import {PageEvent} from '@angular/material';
import {Params} from '@angular/router';

export class AbstractPaginator {
  length: number;
  pageSize = 15;
  pageIndex = 0;
  version: number;

  constructor(response?: HttpResonseInterface) {
    if (response) {
      this.length = response.totalElements;
      this.pageSize = response.size;
      this.pageIndex = response.number;
    }
    this.updateVersion();
  }

  updateFromPageEvent(event: PageEvent) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.updateVersion();
  }

  updateFromParams(params: Params) {
    if (params.pageIndex) { this.pageIndex = params.pageIndex; }
    if (params.pageSize) { this.pageSize = params.pageSize; }
    this.updateVersion();
  }

  reset(){
    this.pageSize = 15;
    this.pageIndex = 0;
  }

  updateVersion(){
    this.version = +new Date();
  }
}
