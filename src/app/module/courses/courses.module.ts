import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesListComponent } from './component/list/courses-list.component';
import { CoursesRoutingModule } from './courses.routing.module';
import {MatPaginatorModule, MatSelectModule, MatTableModule, MatExpansionModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {PipeModule} from '../pipe/pipe.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {CourseMaterialsComponent} from './component/materials/course-materials.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    CoursesRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    PipeModule,
    NgSelectModule,
    MatExpansionModule,
    NgxSpinnerModule
  ],
  declarations: [
    CoursesListComponent, CourseMaterialsComponent
  ],
  exports: [CoursesListComponent, CourseMaterialsComponent]
})
export class CoursesModule { }
