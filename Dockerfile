FROM nginx

RUN apt-get update && apt-get install -y curl gnupg2
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN apt-get install -y bzip2

COPY . /app
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /app

COPY package*.json ./

RUN npm install rimraf -g --verbose
RUN npm install --verbose

RUN npm run build

RUN cp -a dist/. /app/src



