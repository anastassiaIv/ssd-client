import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {HeaderComponent} from './module/header-footer/component/header.component';
import {ScriptLoaderService} from './service/script-loader.service';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  globalBodyClass = 'm-page--loading-non-block m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';
  @ViewChild(HeaderComponent) headerComponent: HeaderComponent;

  constructor(private _script: ScriptLoaderService) {
  }

  ngOnInit() {
    this._script.loadScripts('body',
      [
        'assets/vendors/base/vendors.bundle.js',
        'assets/demo/default/base/scripts.bundle.js'
      ], true);
  }
}
