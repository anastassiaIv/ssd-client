import {forwardRef, Inject, OnInit} from '@angular/core';
import {AppComponent} from '../../../app.component';
import {NavigationService} from '../../../service/navigation.service';
import {ScriptLoaderService} from '../../../service/script-loader.service';

export class AbstractCourseSimilaritiesComponent implements OnInit {

  protected app: AppComponent;
  protected _script: ScriptLoaderService;

  constructor(@Inject(forwardRef(() => AppComponent)) _app: AppComponent, protected navService: NavigationService,
              protected scriptLoaderService: ScriptLoaderService) {
    this.app = _app;
    this._script = scriptLoaderService;
  }

  ngOnInit(): void {
    this.app.headerComponent.setTitleAndSubNav(this.navService.getCourseSuggestionsNavigationSubNavigation());
  }

}
