import {BehaviorSubject} from 'rxjs';

export class AbstractDataSource {

  public total = 0;
  protected loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();
}
